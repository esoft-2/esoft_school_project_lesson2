import React from "react";
import "../card.css"

export const Img = ({
    src,
    alt,
    imgClass
}) => {
    return(
        <img src={src} alt={alt} className={imgClass}/>
    )
}