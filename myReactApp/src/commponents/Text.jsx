import React from "react";
import "../card.css"

export const Text = (props) => {
    return(
        <p className={props.className}>{props.text}</p>
    )
}