import React from 'react'
import '../App.css'
import { Img } from './Img'
import { Text } from './Text'

export const Card = ({
    src,
    alt,
    className,
    text,
    imgClass
}) => {
    return(
        <div className='main-card'>
            <Img src={src} alt={alt} imgClass={imgClass} />
            <Text className={className} text={text} />
        </div>
    )
}