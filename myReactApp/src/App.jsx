import React from 'react'
import './App.css'
import { Text } from './commponents/Text'
import { Card } from './commponents/Card'

function App() {
  return (
    <>
      <Card src="/Img1.jpg" alt="Main img" className="big-txt" text="Данилевич Глеб Станиславович" imgClass="img-for-all"/>

      <Text className="big-txt dop-class" text="Компитенции" />

      <div className='second-card'>
        <Card src="/Img2.jpg" alt="Main img" className="min-txt" text="Работал и настраивал разные дистребутивы Linux" imgClass="mini-img" />
        <Card src="/Img3.jpg" alt="test img" className="min-txt" text="Работал с системами мониторинга, ansible, docker" imgClass="mini-img" />
        <Card src="/Img4.jpg" alt="Main img" className="min-txt" text="Изучал чуть побольше основ (надеюсь) beckand и DB" imgClass="mini-img" />
        <Card src="/Img5.jpg" alt="ops img is not :)" className="min-txt" text="Быстро учусь" imgClass="mini-img" />
      </div>

      <div className='third-card'>
        <Text className="one-text" text="Хочу научится читать код" />
        <Text className="one-text" text="Хочу научится писать поддерживаемый, более правильный и понятный код " />
        <Text className="one-text" text="Хочу изучить более детально Node js и BD" />
        <Text className="one-text" text="Хочу научится работать в команде с другими разрабочиками" />
        <Text className="one-text" text="Понять что нужно было писать технологии компетенци))" />
        <Text className="one-text" text="Изучить React" />
        <Text className="one-text" text="Изучить TS" />
        <Text className="one-text" text="Изучить технологии для тестирования web-приложений" />
        <Text className="one-text" text="Хочу научится придумывать текст" />
        <Text className="one-text" text="Изчуть архитектуру современого web-приложения" />
      </div>

      <Text className="big-txt dop-class" text="Извиняюсь за возможные грамматические ошибки и очень-очень корявые стили, надеюсь это не главные критерии оценивания" />

    </>
  )
}

export default App
